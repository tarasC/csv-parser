TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    csv_parser.c

HEADERS += \
    csv_parser.h

