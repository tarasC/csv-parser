/**
******************************************************************************
* @file    csv_parser.h
* @date    4-June-2019
* @brief   Describes the interface of csv files parser.
*          The parser expects csv file with the numerical values.
* @author  Taras Chornenkyi
******************************************************************************
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#ifndef CSV_PARSER_H
#define CSV_PARSER_H

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

struct csv_file_s{
    // The number of columns in the table.
    // Maximum index that can be accessed is comumns_count -1
    uint32_t columns_count;
    // The number of rows in the table.
    // Maximum index that can be accessed is comumns_count -1
    uint32_t rows_count;
    // Names of the columns. column_names[0] is the name of the frist column
    char** column_names;
    // Values stored as 2D array.
    // Access data[column][row].
    float** data;
};

struct csv_file_s* csv_parse(char* file_name);

#ifdef __cplusplus
}
#endif

#endif // CSV_PARSER_H
