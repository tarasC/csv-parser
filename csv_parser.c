/**
******************************************************************************
* @file    csv_parser.c
* @date    4-June-2019
* @brief   Implementation of csv parser.
* @author  Taras Chornenkyi
******************************************************************************
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "csv_parser.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "stdio.h"

struct csv_file_s* csv_parse(char* file_name)
{
    if(file_name == NULL) {
        printf("Filename is null\n");
        return NULL;
    }

    // Variables
    FILE* file;
    char* buf = NULL;

    // Open file. Put correct filename here
    file = fopen(file_name, "r");
    if(file == NULL) {
        // File wasn't opened
        printf("Can's open file %s\n", file_name);
        return NULL;
    }
    printf("File %s was opened\n", file_name);

    // Get the size of the file
    fseek(file, 0L, SEEK_END);
    size_t sz = (size_t)ftell(file);
    // Go to the beginning of the file again
    fseek(file, 0L, SEEK_SET);

    // Create buffer for the file
    buf = malloc(sz * sizeof(char));
    if(buf == NULL) {
        // Array wasn't created
        printf("Can't allocate memory for temporal buffer. Perhaps, the file is too big.\n");
        return NULL;
    }
    // Read file to the buffer
    fread(buf, sizeof(char), sz, file);
    // Go to start again
    fseek(file, 0L, SEEK_SET);



    // One index for two arrays
    uint32_t columns = 0;
    uint32_t rows = 0;

    // Count columns
    char* tmpcp = buf;
    do {
        if(0 != isdigit(*tmpcp) ||
                *tmpcp == '-' ||
                *tmpcp == '+'){
            // it's a number
            break;
        }

        // it's not a number. Cosnider as a column name
        columns++;
        // Find new value
        tmpcp = strstr(tmpcp, ",") + 1;
    } while(tmpcp != NULL);

    // Count rows
    // tmpcp stores the first number address
    while(tmpcp != NULL) {
        rows++;
        // Find new value
        tmpcp = strstr(tmpcp + 1, "\n");
    };

    // Allocate memory
    struct csv_file_s* csv = (struct csv_file_s*) malloc(sizeof(struct csv_file_s));
    if(csv == NULL) {
        printf("Can't allocate memory for the table\n");
        return NULL;
    }
    csv->columns_count = columns;
    csv->rows_count = rows - 1;

    printf("Columns count: %u\n", csv->columns_count);
    printf("Data rows count: %u\n", csv->rows_count);

    csv->column_names = (char**)malloc(csv->columns_count * sizeof(char*));
    csv->data = (float**)malloc(csv->columns_count * sizeof(float*));
    if(csv->column_names == NULL || csv->data == NULL) {
        printf("Can't allocate memory for the data\n");
        return NULL;
    }

    for(uint32_t i = 0; i < csv->columns_count; i++) {
        csv->data[i] = (float*)malloc(csv->rows_count * sizeof(float));
        if(csv->data[i] == NULL) {
            printf("Can't allocate memory for the data[%u]\n", i);
            return NULL;
        }
    }

    uint32_t column_index = 0;

    memset(buf, 0,  sz * sizeof(char));
    fgets(buf,  (int)sz * (int)sizeof(char), file);
    // Get the first token
    char* token = strtok(buf, ",");
    // Walk through the tokens to parse columns names
    while( token != NULL ) {

        if(0 == isdigit(token[0]) &&
                token[0] != '-' &&
                token[0] != '+' &&
                token[0] != '.') {
            // The value isn't digit so this is a column name
            char* tmp = NULL;
            char* tmp1 = NULL;
            if(NULL != (tmp = strstr(token, "\n"))) {
                *tmp = '\0';
            }

            uint32_t len = strlen(token);
            if(NULL == (csv->column_names[column_index] = (char*)malloc(len+1))) {
                printf("Can't allocate column name\n");
                return NULL;
            }
            strcpy(csv->column_names[column_index], token);
            // printf("Column %u name is %s\n", column_index, csv->column_names[column_index]);
        }

        if(++column_index >= csv->columns_count) {
            break;
        }
        // Try to get next token
        token = strtok(NULL, ",");
    }

    for(uint32_t row = 0; row < csv->rows_count; row++) {
        if(NULL == fgets(buf, 1024, file)) {
            printf("Not enough data\n");
        }
        // Get the first token
        token = strtok(buf, ",");
        // Walk through the tokens to parse numbers
        column_index = 0;
        while( token != NULL ) {
            csv->data[column_index][row] = (float)atof(token);
            if(++column_index >= csv->rows_count) {
                break;
            }
            // Try to get next token
            token = strtok(NULL, ",");
        }
    }

    // Now we have all data in the buffer so can close the file
    fclose(file);
    return csv;
}



